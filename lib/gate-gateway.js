const { default: Axios } = require('axios')
const { createHmac, createHash } = require('crypto')
const { config: dotenv } = require('dotenv')

dotenv()

const {
  GATE_WAY_HOST = 'https://api-way.mercurydev.tk',
  SECRET_PUBLIC_ID = '',
  SECRET_KEY = '',
  PAIRING_CODE = ''
} = process.env

/**
 * Gate Gateway Class
 */
class Gateway {
  constructor (options) {
    this.options = {
      baseUrl: GATE_WAY_HOST,
      apiKey: SECRET_PUBLIC_ID,
      apiSecret: SECRET_KEY,
      pairingCode: PAIRING_CODE,
      ...options
    }
    this.request = Axios.create({
      baseURL: this.options.baseUrl
    })

    if (this.options.pairingCode) {
      console.log('Using pairing code')
      this.request.interceptors.request.use(this._pairingRequestInterceptor.bind(this))
    } else {
      this.request.interceptors.request.use(this._apiKeyRequestInterceptor.bind(this))
    }
  }

  _pairingRequestInterceptor (req) {
    console.log(this.options.baseUrl, req.url)
    req.headers['pairing-code'] = this.options.pairingCode

    const hashGenerator = createHash('sha512')
    hashGenerator.update(this.options.pairingCode)
    const secretHash = hashGenerator.digest('hex')

    const data = JSON.stringify(req.data || {})
    const hmac = createHmac('sha256', secretHash)
    hmac.update(data, 'utf8')
    const signature = hmac.digest('hex')
    req.headers.signature = signature
    return req
  }

  _apiKeyRequestInterceptor (req) {
    console.log(this.options.baseUrl, req.url)
    req.headers['x-api-key'] = this.options.apiKey

    const hashGenerator = createHash('sha512')
    hashGenerator.update(this.options.apiSecret)
    const secretHash = hashGenerator.digest('hex')

    const data = JSON.stringify(req.data || {})
    const hmac = createHmac('sha256', secretHash)
    hmac.update(data, 'utf8')
    const signature = hmac.digest('hex')
    req.headers.signature = signature
    return req
  }

  postCheckout (data) {
    return this.request.post('/checkout', data)
  }

  putCheckout (data) {
    return this.request.put('/checkout', data)
  }

  getTransaction (uuid) {
    return this.request.get(`/transaction/${uuid}`)
  }

  postInvoice (data) {
    return this.request.post('/invoice', data)
  }
}

module.exports = Gateway
