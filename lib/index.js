const { resolve } = require('path')
const yargs = require('yargs')

const { argv } = yargs
  .commandDir(resolve(__dirname, 'cmds'))
  .demandCommand()
  .help()

console.log(argv.$0)
