const Gateway = require('../gate-gateway')

module.exports = {
  command: 'invoice',
  desc: 'Create invoice',
  builder: {
  },
  handler: (argv) => {
    const gateway = new Gateway()
    gateway
      .postInvoice({
        client: {
          /*
          address: 'Barcelona',
          country_id: 199,
          province_id: 950,
          city: 'Barcelona',
          zip_code: '28231',
          */
          email: 'marco@mercury.cash'
        },
        invoice_number: '0000001',
        isBuyerAddressAdded: true,
        currency: 'USD',
        due_date: 20,
        amount: 50,
        sendEmail: true,
        cron_expression: '* * * 1/1 * *',
        products: [
          {
            productId: '1ef85f49-8851-497e-a2c1-a250844db521',
            quantity: 1,
            amount: 50
          }
        ]
      })
      .then(({ data }) => {
        console.log(data)
      })
      .catch(err => {
        console.error('\nERROR', err.response.data)
      })
  }
}
