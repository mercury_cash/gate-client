const Gateway = require('../gate-gateway')

module.exports = {
  command: 'status',
  desc: 'Get transaction status',
  builder: {
    u: {
      alias: 'uuid',
      describe: 'Transaction UUID',
      coerce: String,
      demandOption: true
    }
  },
  handler: (argv) => {
    const gateway = new Gateway()
    gateway
      .getTransaction(argv.u)
      .then(({ data }) => {
        console.log(data)
      })
      .catch(err => {
        console.error('\nERROR', err.message, err.response.data)
      })
  }
}
