const Gateway = require('../gate-gateway')

module.exports = {
  command: 'start',
  desc: 'Start checkout process',
  builder: {
    u: {
      alias: 'uuid',
      describe: 'Transaction UUID',
      coerce: String,
      demandOption: true
    }
  },
  handler: (argv) => {
    const gateway = new Gateway()
    gateway
      .putCheckout({
        uuid: argv.u
      })
      .then(({ data }) => {
        console.log(data)
      })
      .catch(err => {
        console.error('\nERROR', err.response.data)
      })
  }
}
