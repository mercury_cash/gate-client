const Gateway = require('../gate-gateway')

module.exports = {
  command: 'create',
  desc: 'Create transaction',
  builder: {
    c: {
      alias: 'crypto',
      coerce: String,
      default: 'ETH',
      describe: 'Transaction crypto.',
      choices: ['ETH', 'DASH', 'BTC'],
      demandOption: true
    },
    f: {
      alias: 'fiat',
      coerce: String,
      default: 'USD',
      describe: 'Transaction FIAT,',
      choices: ['USD', 'EUR'],
      demandOption: true
    },
    a: {
      alias: 'amount',
      coerce: Number,
      describe: 'Transaction Amount',
      demandOption: true
    },
    t: {
      alias: 'tip',
      coerce: Number,
      default: 0,
      describe: 'Transaction Tip',
      demandOption: true
    },
    e: {
      alias: 'email',
      coerce: String,
      describe: 'Reference email',
      demandOption: true
    },
    o: {
      alias: 'order_number',
      describe: 'Transaction reference number',
      coerce: String,
      demandOption: true
    }
  },
  handler: (argv) => {
    console.time('create')
    const gateway = new Gateway()
    gateway
      .postCheckout({
        crypto: argv.c,
        fiat: argv.f,
        amount: argv.a,
        tip: argv.t,
        email: argv.e,
        order_number: argv.o
      })
      .then(({ data }) => {
        console.log(data)
      })
      .catch(err => {
        console.error('\nERROR', err.response.data)
      })
      .finally(() => {
        console.timeEnd('create')
      })
  }
}
