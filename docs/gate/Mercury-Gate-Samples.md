---
tags: [gate, samples, mercury gate]
---

# Mercury-Gate-Samples

Under this repo you will be able to see real examples that can be executed in the Mercury Gate API.

To clone this repo and start playing with the examples please click [here](https://bitbucket.org/mercury_cash/gate-client/src/master/)

## Prepare Repo

```shell
npm i 
```

## Prerequisites

You need to have a public key and a secret created in _GATE_

you must to add a _.env_ file with the secrets. something like

```env

SECRET_PUBLIC_ID=852d9167c7899805c2d80687f632b36a097e38e8fe0f91e22feeb4064a7d822b
SECRET_KEY=TWJTJ9vvsUY8uhAc8+r4eDoYd1X+c2haAXcp/tGsUa8=

```

__note__ This keys are not valid

## Commands

```shell

$ node .

Commands:
  create   Create transaction
  invoice  Create invoice
  start    Start checkout process
  status   Get transaction status

Options:
  --version  Show version number                                       [boolean]
  --help     Show help                                                 [boolean]

Not enough non-option arguments: got 0, need at least 1

```